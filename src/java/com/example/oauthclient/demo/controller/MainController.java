package com.example.oauthclient.demo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class MainController {

	private static Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@Autowired
	private RestTemplate rt;

	@Value("${external.rest.url}")
	private String url;
	
	@RequestMapping(value="/message")
	public String getMessage() {
		ResponseEntity<String> response = null;
		logger.info("Before calling the rest method");
		response = this.rt.getForEntity(this.url, String.class);
		logger.info("Aftger calling the rest method");
		
		logger.info("status code: " + response.getStatusCodeValue());
		logger.info("reason phase: " + response.getStatusCode().getReasonPhrase());
		logger.info("body: " + response.getBody());
		return response.getBody();
	}
	
	@Value("${external2.rest.clientid}")
	private String clientId;
	
	@Value("${external2.rest.clientsecretid}")
	private String clientSecretId;
	
	@Value("${external2.rest.granttype}")
	private String grantType;
	
	@Value("${external2.rest.tokenurl}")
	private String tokenUrl;
	
	@Value("${external2.rest.url}")
	private String oauthUrl;
	
	@Value("${external2.rest.scopes}")
	private List<String> scopes;
	
	@RequestMapping(value="/messageoauth")
	public String getMessageoauth() {
		ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
		resourceDetails.setClientId( this.clientId );
		resourceDetails.setClientSecret( this.clientSecretId );
		resourceDetails.setGrantType( this.grantType );
		resourceDetails.setAccessTokenUri( this.tokenUrl );
		resourceDetails.setScope( this.scopes );
		
		OAuth2RestTemplate ort = new OAuth2RestTemplate(resourceDetails);
		OAuth2AccessToken token = ort.getAccessToken();
		
		logger.info("access token: " + token.getValue());
		logger.info("token type: " + token.getTokenType());
		logger.info("expires in: " + token.getExpiresIn());
		logger.info("expiration date:" + token.getExpiration());
		logger.info("scopes: " + token.getScope());
		logger.info("refresh token: " + ((token.getRefreshToken()==null)?token.getRefreshToken():token.getRefreshToken().getValue()));
		
		ResponseEntity<String> response = null;
		logger.info("Before calling the rest method");
		
	    response = ort.getForEntity( this.oauthUrl , String.class);
		logger.info("After calling the rest method");
		
		logger.info("status code: " + response.getStatusCodeValue());
		logger.info("reason phase: " + response.getStatusCode().getReasonPhrase());
		logger.info("body: " + response.getBody());
		return response.getBody();
	}
	
}
